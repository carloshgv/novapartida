<?php
defined('JPATH_BASE') or die;

$item    = $displayData['item'];
$params  = $displayData['params'];
$info    = $displayData['info'];
$useDefList = $displayData['useDefList'];
?>

<div class="page-header">
	<div class="article-header-info-block">
		<h2 itemprop="name">
			<?php if ($params->get('show_title')) : ?>
				<?php echo $item->title; ?>
			<?php endif; ?>
		</h2>

		<div class="introtext">
			<?php echo $item->introtext; ?>
		</div>

		<?php if ($useDefList && ($info == 0 || $info == 2)) : ?>
			<?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $item, 'params' => $params, 'position' => 'above')); ?>
		<?php endif; ?>

		<?php if ($item->state == 0) : ?>
			<span class="label label-warning"><?php echo JText::_('JUNPUBLISHED'); ?></span>
		<?php endif; ?>
		<?php if (strtotime($item->publish_up) > strtotime(JFactory::getDate())) : ?>
			<span class="label label-warning"><?php echo JText::_('JNOTPUBLISHEDYET'); ?></span>
		<?php endif; ?>
		<?php if ((strtotime($item->publish_down) < strtotime(JFactory::getDate())) && $item->publish_down != JFactory::getDbo()->getNullDate()) : ?>
			<span class="label label-warning"><?php echo JText::_('JEXPIRED'); ?></span>
		<?php endif; ?>
	</div>
</div>
