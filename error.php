<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

if (!isset($this->error))
{
	$this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
	$this->debug = false;
}

// Get language and direction
$doc             = JFactory::getDocument();
$app             = JFactory::getApplication();
$this->language  = $doc->language;
$this->direction = $doc->direction;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->baseurl; ?>/templates/novapartida/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->baseurl; ?>/templates/novapartida/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->baseurl; ?>/templates/novapartida/favicon-16x16.png">
	<title><?php echo $this->error->getCode(); ?> - <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></title>
	<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/novapartida/css/error.css" type="text/css" />
	<?php if ($app->get('debug_lang', '0') == '1' || $app->get('debug', '0') == '1') : ?>
		<link rel="stylesheet" href="<?php echo $this->baseurl ?>/media/cms/css/debug.css" type="text/css" />
	<?php endif; ?>
</head>
<body>
	<h1><?php echo $this->error->getCode(); ?></h1>
	<h2><?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></h2>
	<div id="screen">
		<img src="<?php echo $this->baseurl; ?>/templates/novapartida/images/screen-big.png"/>
		<div id="game-screen"></div>
	</div>

	<map name="gamepadmap">
		<area id="start-button" shape="rect" href="javascript:void(0);" coords="221, 114, 308, 133"/>
	</map>

	<img id="gamepad" src="<?php echo $this->baseurl; ?>/templates/novapartida/images/gamepad-big.png" usemap="#gamepadmap"/>

	<script>
		var startButton = document.getElementById('start-button');

		function start() {
			console.log('loading...');
			startButton.removeEventListener('click', start);
			document.getElementById('gamepad').className = 'out';
			setTimeout(function() {
				document.getElementById('game-screen').style.opacity = 1;
			}, 600);

			var body = document.getElementsByTagName('body')[0];

			window.gameBasePath = '/mini-space-shooter';

			var phaser = document.createElement('script');
			phaser.type = 'text/javascript';
			phaser.src = window.gameBasePath + '/phaser.min.js';
			body.appendChild(phaser);

			phaser.addEventListener('load', function() {
				var game = document.createElement('script');
				game.type = 'text/javascript';
				game.src = window.gameBasePath + '/game.js';
				body.appendChild(game);
			});
		}

		startButton.addEventListener('click', start);
	</script>
</body>
</html>
