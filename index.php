<?php defined( '_JEXEC' ) or die( 'Restricted access' );

$hasSidebar = $this->countModules("position-2");

if($hasSidebar)
    $contentCols = "col-md-8";
else
    $contentCols = "col-md-12";

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
   xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >

<head>
   <meta name="twitter:card" content="summary_large_image">
   <meta name="twitter:site" content="@novapartida">
   <jdoc:include type="head" />

   <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-57x57.png">
   <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-60x60.png">
   <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-72x72.png">
   <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-76x76.png">
   <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-114x114.png">
   <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-120x120.png">
   <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-144x144.png">
   <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-152x152.png">
   <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->baseurl; ?>/templates/novapartida/apple-icon-180x180.png">
   <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->baseurl; ?>/templates/novapartida/android-icon-192x192.png">
   <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->baseurl; ?>/templates/novapartida/favicon-32x32.png">
   <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->baseurl; ?>/templates/novapartida/favicon-96x96.png">
   <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->baseurl; ?>/templates/novapartida/favicon-16x16.png">
   <link rel="manifest" href="<?php echo $this->baseurl; ?>/templates/novapartida/manifest.json">
   <meta name="msapplication-TileColor" content="#ffffff">
   <meta name="msapplication-TileImage" content="<?php echo $this->baseurl; ?>/templates/novapartida/ms-icon-144x144.png">
   <meta name="theme-color" content="#ffffff">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/system/css/system.css" type="text/css" />
   <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/system/css/general.css" type="text/css" />
   <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/novapartida/css/bootstrap.min.css" type="text/css" />
   <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/novapartida/css/template.css?v=15" type="text/css" />
</head>

<body>
    <div class="header">
        <jdoc:include type="modules" name="header"/>
    </div>

    <div class="main-column-wrapper">
        <div class="body main-column">
            <hr class="top-border"/>
            <div class="main">
                <jdoc:include type="modules" name="top"/>
                <div class="content">
                    <div class="row">
                        <div class="<?php echo $contentCols; ?>">
                            <jdoc:include type="component" />
                        </div>
                        <?php if($hasSidebar) : ?>
                        <div class="vertical-separator for-sidebar"></div>
                        <div class="col-md-4">
                            <div class="sidebar">
                                <jdoc:include type="modules" name="position-2" style="html5"/>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer main-column">
            <hr class="top-border"/>
            <div class="row menu-row clearfix">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="gamepad-wrapper">
                                <img width="208" src="<?php echo $this->baseurl; ?>/templates/novapartida/images/gamepad.png" alt="novapartida" title="novapartida" onclick="window.location='novapartida'"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <jdoc:include type="modules" name="footer-1" style="html5"/>
                                </div>
                                <div class="col-md-6">
                                    <jdoc:include type="modules" name="footer-2" style="html5"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <jdoc:include type="modules" name="footer-3" style="html5"/>
                </div>
            </div>
            <div class="copyright">© <?php echo date("Y"); ?> novapartida</div>
        </div>
        <div class="footer-ender main-column">
        </div>
    </div>
    <jdoc:include type="modules" name="position-5"/>
    <script>
        jQuery(function() {
            var $ = jQuery;
            $('.vertical-separator.for-sidebar')
                .height($('.content').outerHeight());

            setTimeout(function() {
                $('.vertical-separator.for-sidebar')
                    .height($('.content').outerHeight());
            }, 500);
        });
    </script>
</body>

</html>
